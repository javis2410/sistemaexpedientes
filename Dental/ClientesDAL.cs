﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Dental
{
    public class ClientesDAL
    {
        public static int Agregar(Cliente pCliente)
        {

            int retorno = 0;

            MySqlCommand comando = new MySqlCommand(string.Format("Insert into Pacientes (Nombre, Apellido_P,Apellido_M,Edad,Direccion,Localidad,Telefono,Sexo,Alergias,Cuales,Antecedentes,Medicamentos,Tratamiento,Anticipo,Total) values ('{0}','{1}','{2}', '{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}')",
                pCliente.Nombre, pCliente.Apellido_P, pCliente.Apellido_M,pCliente.Edad,pCliente.Direccion,pCliente.Localidad,pCliente.Telefono,pCliente.Sexo, pCliente.Alergias,pCliente.Cuales,pCliente.Antecedentes, pCliente.Medicamentos,pCliente.Tratamiento,pCliente.Anticipo,pCliente.Total), BdComun.ObtenerConexion());
            retorno = comando.ExecuteNonQuery();
            return retorno;
        }
        public static List<Cliente> Buscar(string pNombre, string pApellido_P)
        {
            List<Cliente> _lista = new List<Cliente>();

            MySqlCommand _comando = new MySqlCommand(String.Format(
           "SELECT Idpacientes, Nombre, Apellido_P, Apellido_M,Direccion,Edad,Localidad,Telefono,Sexo,Alergias,Cuales,Antecedentes,Medicamentos,Tratamiento,Anticipo,Total FROM pacientes  where Nombre ='{0}' or Apellido_P='{1}'", pNombre, pApellido_P), BdComun.ObtenerConexion());
            MySqlDataReader _reader = _comando.ExecuteReader();
            while (_reader.Read())
            {
                Cliente pCliente = new Cliente();
                pCliente.Idpacientes = _reader.GetInt32(0);
                pCliente.Nombre = _reader.GetString(1);
                pCliente.Apellido_P = _reader.GetString(2);
                pCliente.Apellido_M = _reader.GetString(3);
                pCliente.Edad = _reader.GetInt32(5);
                pCliente.Direccion = _reader.GetString(4);
                pCliente.Localidad = _reader.GetString(6);
                pCliente.Telefono = _reader.GetString(7);
                pCliente.Sexo = _reader.GetString(8);
                pCliente.Alergias = _reader.GetString(9);
                pCliente.Cuales = _reader.GetString(10);
                pCliente.Antecedentes = _reader.GetString(11);
                pCliente.Medicamentos = _reader.GetString(12);
                pCliente.Tratamiento = _reader.GetString(13);
                pCliente.Anticipo = _reader.GetFloat(14);
                pCliente.Total = _reader.GetFloat(15);


                _lista.Add(pCliente);
            }

            return _lista;
        }
        public static Cliente ObtenerCliente(int pIdpacientes)
        {
            Cliente pCliente = new Cliente();
            MySqlConnection conexion = BdComun.ObtenerConexion();

            MySqlCommand _comando = new MySqlCommand(String.Format("SELECT Idpacientes, Nombre, Apellido_P,Apellido_M,Edad,Direccion,Localidad,Telefono,Sexo,Alergias,Cuales,Antecedentes,Medicamentos,Tratamiento,Anticipo,Total FROM pacientes where Idpacientes={0}", pIdpacientes), conexion);
            MySqlDataReader _reader = _comando.ExecuteReader();
            while (_reader.Read())
            {
                pCliente.Idpacientes = _reader.GetInt32(0);
                pCliente.Nombre = _reader.GetString(1);
                pCliente.Apellido_P = _reader.GetString(2);
                pCliente.Apellido_M = _reader.GetString(3);
                pCliente.Edad = _reader.GetInt32(4);
                pCliente.Direccion = _reader.GetString(5);
                pCliente.Localidad = _reader.GetString(6);
                pCliente.Telefono = _reader.GetString(7);
                pCliente.Sexo = _reader.GetString(8);
                pCliente.Alergias = _reader.GetString(9);
                pCliente.Cuales = _reader.GetString(10);
                pCliente.Antecedentes = _reader.GetString(11);
                pCliente.Medicamentos = _reader.GetString(12);
                pCliente.Tratamiento = _reader.GetString(13);
                pCliente.Anticipo = _reader.GetFloat(14);
                pCliente.Total = _reader.GetFloat(15);

            }

            conexion.Close();
            return pCliente;

        }
        public static int Actualizar(Cliente pCliente)
        {
            int retorno = 0;
            MySqlConnection conexion = BdComun.ObtenerConexion();

            MySqlCommand comando = new MySqlCommand(string.Format("Update pacientes set Nombre='{0}', Apellido_P='{1}', Apellido_M='{2}',Edad='{3}',Direccion='{4}',Localidad='{5}',Telefono='{6}',Sexo='{7}',Alergias='{8}',Cuales='{9}',Antecedentes='{10}',Medicamentos='{11}',Tratamiento='{12}',Anticipo='{13}',Total='{14}' where Idpacientes={15}",
                pCliente.Nombre, pCliente.Apellido_P, pCliente.Apellido_M, pCliente.Edad, pCliente.Direccion, pCliente.Localidad, pCliente.Telefono, pCliente.Sexo, pCliente.Alergias, pCliente.Cuales, pCliente.Antecedentes, pCliente.Medicamentos, pCliente.Tratamiento, pCliente.Anticipo, pCliente.Total, pCliente.Idpacientes), conexion);

            retorno = comando.ExecuteNonQuery();
            conexion.Close();

            return retorno;


        }
        public static int Eliminar(int pIdpacientes)
        {
            int retorno = 0;
            MySqlConnection conexion = BdComun.ObtenerConexion();

            MySqlCommand comando = new MySqlCommand(string.Format("Delete From pacientes where Idpacientes={0}", pIdpacientes), conexion);

            retorno = comando.ExecuteNonQuery();
            conexion.Close();

            return retorno;

        }


    }
}
