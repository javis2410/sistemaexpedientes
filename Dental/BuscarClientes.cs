﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Dental
{
    public partial class BuscarClientes : Form
    {
        public BuscarClientes()
        {
            InitializeComponent();
        }
        public Cliente ClienteSeleccionado { get; set; }
        private void Buscar_Click(object sender, EventArgs e)
        {
            dgvBuscar.DataSource = ClientesDAL.Buscar(txt1.Text, txt2.Text);
        }

        private void Aceptar_Click(object sender, EventArgs e)
        {
            if(dgvBuscar.SelectedRows.Count == 1)
            {
                int id = Convert.ToInt32(dgvBuscar.CurrentRow.Cells[0].Value);
                ClienteSeleccionado = ClientesDAL.ObtenerCliente(id);

                this.Close();
            }
            else
                MessageBox.Show("debe de seleccionar una fila");
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
