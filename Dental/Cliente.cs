﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dental
{
    public class Cliente
    {
        public int Idpacientes { get; set; }
        public string Nombre { get; set; }
        public string Apellido_P { get; set; }
        public string Apellido_M { get; set; }
        public int Edad { get; set; }
        public string Direccion { get; set; }
        public string Localidad { get; set; }
        public string Telefono { get; set; }
        public string Sexo { get; set; }
        public string Alergias { get; set; }
        public string Cuales { get; set; }
        public string Antecedentes { get; set; }
        public string Medicamentos { get; set; }
        public string Tratamiento { get; set; }
        public float Anticipo { get; set; }
        public float Total { get; set; }




        public Cliente() { }

        public Cliente(int pIdpacientes, string pNombre, string pApellido_P,string pApellido_M, int pEdad, string pDireccion,string pLocalidad,string pTelefono, string pSexo, string pAlergias, string pCuales, string pAntecedentes, string pMedicamentos,string pTratamiento, float pAnticipo, float pTotal)
        {
            this.Idpacientes = pIdpacientes;
            this.Nombre = pNombre;
            this.Apellido_P = pApellido_P;
            this.Apellido_M = pApellido_M;
            this.Edad = pEdad;
            this.Direccion = pDireccion;
            this.Localidad = pLocalidad;
            this.Telefono = pTelefono;
            this.Sexo = pSexo;
            this.Alergias = pAlergias;
            this.Cuales = pCuales;
            this.Antecedentes = pAntecedentes;
            this.Medicamentos = pMedicamentos;
            this.Tratamiento = pTratamiento;
            this.Anticipo = pAnticipo;
            this.Total = pTotal;
        
            
        }
    }
}
