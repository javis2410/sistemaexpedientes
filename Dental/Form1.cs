﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dental
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public Cliente ClienteActual { get; set; }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt1.Text) || string.IsNullOrWhiteSpace(txt2.Text) || string.IsNullOrWhiteSpace(txt3.Text) || string.IsNullOrWhiteSpace(txt4.Text)
                || string.IsNullOrWhiteSpace(txt5.Text) || string.IsNullOrWhiteSpace(txt6.Text) || string.IsNullOrWhiteSpace(txt7.Text) || string.IsNullOrWhiteSpace(txt8.Text)
                || string.IsNullOrWhiteSpace(txt9.Text) || string.IsNullOrWhiteSpace(txt10.Text) || string.IsNullOrWhiteSpace(txt11.Text) || string.IsNullOrWhiteSpace(txt12.Text)
                || string.IsNullOrWhiteSpace(txt13.Text) || string.IsNullOrWhiteSpace(cmb1.Text) || string.IsNullOrWhiteSpace(cmb2.Text))


                MessageBox.Show("Hay Uno o mas Campos Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {
                Cliente pCliente = new Cliente();
                pCliente.Nombre = txt1.Text.Trim();
                pCliente.Apellido_P = txt2.Text.Trim();
                pCliente.Apellido_M = txt3.Text.Trim();
                pCliente.Edad = Int32.Parse(txt4.Text);
                pCliente.Direccion = txt5.Text.Trim();
                pCliente.Localidad = txt6.Text.Trim();
                pCliente.Telefono = txt7.Text.Trim();
                pCliente.Sexo = cmb2.Text.Trim();
                pCliente.Alergias = cmb1.Text.Trim();
                pCliente.Cuales = txt8.Text.Trim();
                pCliente.Antecedentes = txt9.Text.Trim();
                pCliente.Medicamentos = txt10.Text.Trim();
                pCliente.Tratamiento = txt13.Text.Trim();
                pCliente.Anticipo = float.Parse(txt11.Text);
                pCliente.Total = float.Parse(txt12.Text);



                int resultado = ClientesDAL.Agregar(pCliente);
                if (resultado > 0)
                {
                    MessageBox.Show("Paciente Guardado Con Exito!!", "Guardado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    Deshabilitar();

                }
                else
                {
                    MessageBox.Show("No se pudo guardar los datos del Paciente", "Fallo!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
        }

    

        private void Form1_Load(object sender, EventArgs e)
        {
            Deshabilitar();
        }

      

        private void button2_Click_1(object sender, EventArgs e)
        {
            BuscarClientes buscar = new BuscarClientes();
            buscar.ShowDialog();
            if (buscar.ClienteSeleccionado != null)
            {
                ClienteActual = buscar.ClienteSeleccionado;
                txt1.Text = buscar.ClienteSeleccionado.Nombre;
                txt2.Text = buscar.ClienteSeleccionado.Apellido_P;
                txt3.Text = buscar.ClienteSeleccionado.Apellido_M;
                txt4.Text = Convert.ToString(buscar.ClienteSeleccionado.Edad);
                txt5.Text = buscar.ClienteSeleccionado.Direccion;
                cmb2.Text = buscar.ClienteSeleccionado.Sexo;
                txt6.Text = buscar.ClienteSeleccionado.Localidad;
                txt7.Text = buscar.ClienteSeleccionado.Telefono;
                cmb1.Text = buscar.ClienteSeleccionado.Alergias;
                txt8.Text = buscar.ClienteSeleccionado.Cuales;
                txt10.Text = buscar.ClienteSeleccionado.Medicamentos;
                txt9.Text = buscar.ClienteSeleccionado.Antecedentes;
                txt13.Text = buscar.ClienteSeleccionado.Tratamiento;
                txt11.Text = Convert.ToString(buscar.ClienteSeleccionado.Anticipo);
                txt12.Text = Convert.ToString(buscar.ClienteSeleccionado.Total);
                btnActualizar.Enabled = true;
                btnEliminar.Enabled = true;
                Habilitar();
                btnGuardar.Enabled = false;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txt1.Text) || string.IsNullOrWhiteSpace(txt2.Text) || string.IsNullOrWhiteSpace(txt3.Text) || string.IsNullOrWhiteSpace(txt4.Text)
                || string.IsNullOrWhiteSpace(txt5.Text) || string.IsNullOrWhiteSpace(txt6.Text) || string.IsNullOrWhiteSpace(txt7.Text) || string.IsNullOrWhiteSpace(txt8.Text)
                || string.IsNullOrWhiteSpace(txt9.Text) || string.IsNullOrWhiteSpace(txt10.Text) || string.IsNullOrWhiteSpace(txt11.Text) || string.IsNullOrWhiteSpace(txt12.Text)
                || string.IsNullOrWhiteSpace(txt13.Text) || string.IsNullOrWhiteSpace(cmb1.Text) || string.IsNullOrWhiteSpace(cmb2.Text))


                MessageBox.Show("Hay Uno o mas Campos Vacios!", "Campos Vacios!!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
            {

                Cliente pCliente = new Cliente();
                pCliente.Nombre = txt1.Text.Trim();
                pCliente.Apellido_P = txt2.Text.Trim();
                pCliente.Apellido_M = txt3.Text.Trim();
                pCliente.Edad = Int32.Parse(txt4.Text);
                pCliente.Direccion = txt5.Text.Trim();
                pCliente.Localidad = txt6.Text.Trim();
                pCliente.Telefono = txt7.Text.Trim();
                pCliente.Sexo = cmb2.Text.Trim();
                pCliente.Alergias = cmb1.Text.Trim();
                pCliente.Cuales = txt8.Text.Trim();
                pCliente.Antecedentes = txt9.Text.Trim();
                pCliente.Medicamentos = txt10.Text.Trim();
                pCliente.Tratamiento = txt13.Text.Trim();
                pCliente.Anticipo = float.Parse(txt11.Text);
                pCliente.Total = float.Parse(txt12.Text);
                pCliente.Idpacientes = ClienteActual.Idpacientes;
                if (ClientesDAL.Actualizar(pCliente) > 0)
                {
                    MessageBox.Show("Los datos del paciente se actualizaron", "Datos Actualizados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    Deshabilitar();

                }
                else
                {
                    MessageBox.Show("No se pudo actualizar", "Error al Actualizar", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Esta Seguro que desea eliminar el Cliente Actual", "Estas Seguro??", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (ClientesDAL.Eliminar(ClienteActual.Idpacientes) > 0)
                {
                    MessageBox.Show("Cliente Eliminado Correctamente!", "Cliente Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpiar();
                    Deshabilitar();
                 
                }
                else
                {
                    MessageBox.Show("No se pudo eliminar el Cliente", "Cliente No Eliminado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
                MessageBox.Show("Se cancelo la eliminacion", "Eliminacion Cancelada", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        void Limpiar()
        {
            txt1.Clear();
            txt2.Clear();
            txt3.Clear();
            txt4.Clear();
            txt5.Clear();
            txt6.Clear();
            txt7.Clear();
            txt8.Clear();
            txt9.Clear();
            txt10.Clear();
            txt11.Clear();
            txt12.Clear();
            txt13.Clear();
            cmb1.SelectedIndex = -1;
            cmb2.SelectedIndex = -1;

        }
        void Habilitar()
        {
            txt1.Enabled = true;
            txt2.Enabled = true;
            txt3.Enabled = true;
            txt4.Enabled = true;
            txt5.Enabled = true;
            txt6.Enabled = true;
            txt7.Enabled = true;
            txt8.Enabled = true;
            txt9.Enabled = true;
            txt10.Enabled = true;
            txt11.Enabled = true;
            txt12.Enabled = true;
            txt13.Enabled = true;
            cmb1.Enabled = true;
            cmb2.Enabled = true;
            btnGuardar.Enabled = true;


        }
        void Deshabilitar()
        {
            txt1.Enabled = false;
            txt2.Enabled = false;
            txt3.Enabled = false;
            txt4.Enabled = false;
            txt5.Enabled = false;
            txt6.Enabled = false;
            txt7.Enabled = false;
            txt8.Enabled = false;
            txt9.Enabled = false;
            txt10.Enabled = false;
            txt11.Enabled = false;
            txt12.Enabled = false;
            txt13.Enabled = false;
            cmb1.Enabled = false;
            cmb2.Enabled = false;
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = false;
            btnActualizar.Enabled = false;
            btnBuscar.Enabled = true;
            btnNuevo.Enabled = true;
           

            

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Limpiar();
            Habilitar();
        }
    }
}
